//
//  Book+CoreDataProperties.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 13/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//
// Created with (Editor\Create NSManagedObject Subclass) then in group select the folder

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var author: String?
    @NSManaged public var name: String?
    @NSManaged public var nPages: Int16

}
