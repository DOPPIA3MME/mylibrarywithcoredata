//
//  ModifyViewController.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 15/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class ModifyViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
	
	@IBOutlet weak var byTextField: UITextField!
	@IBOutlet weak var attTextField1: DesignableUITextField!
	@IBOutlet weak var attTextField2: DesignableUITextField!
	@IBOutlet weak var saveButton: UIButton!
	
	
	let byPickerView: UIPickerView = UIPickerView()
	var pickerData: [String] = ["name","author","nPages"]
	var pickerImages: [String] = ["book","person","number.circle"]
	var elementPicked = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		byPickerView.delegate = self
		byPickerView.dataSource = self
		attTextField1.delegate = self
		attTextField2.delegate = self
	
		attTextField1.placeholder = pickerData[0]
		attTextField2.placeholder = pickerData[0]
		
		initElements()
		
		byTextField.inputView = byPickerView
		byTextField.text = pickerData[0]
		byTextField.tintColor = UIColor.clear
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
		
		//Uncomment the line below if you want the tap not not interfere and cancel other interactions.
		//tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
		
		
		let left = UISwipeGestureRecognizer(target : self, action : #selector(leftSwipe))
		left.direction = .left
		view.addGestureRecognizer(left)
		
		let right = UISwipeGestureRecognizer(target : self, action : #selector(rightSwipe))
		right.direction = .right
		view.addGestureRecognizer(right)
		
		let up = UISwipeGestureRecognizer(target : self, action : #selector(upSwipe))
		up.direction = .up
		view.addGestureRecognizer(up)
		
		let down = UISwipeGestureRecognizer(target : self, action : #selector(downSwipe))
		down.direction = .down
		view.addGestureRecognizer(down)
		
	}
	
	
//MARK: - Text Field
	//Grazie al delegate posso usare questa funzione
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		var text1 = attTextField1.text
		var text2 = attTextField2.text
		
		if textField == attTextField1 {
			text1 = (attTextField1.text! as NSString).replacingCharacters(in: range, with: string)
		}
		else if textField == attTextField2 {
			text2 = (attTextField2.text! as NSString).replacingCharacters(in: range, with: string)
		}
		
		if text1!.isEmpty || text2!.isEmpty {
			saveButton.isEnabled = false
			saveButton.alpha = 0.5
		} else {
			saveButton.isEnabled = true
			saveButton.alpha = 1.0
		}
		
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		
		if textField == attTextField1{
			attTextField1.layer.borderWidth = 2
		}
		else{
			attTextField2.layer.borderWidth = 2
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		
		if textField == attTextField1{
			attTextField1.layer.borderWidth = 0
		}
		else{
			attTextField2.layer.borderWidth = 0
		}
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	
//MARK: - Picker
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return pickerData.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return pickerData[row]
	}
	
	// Capture the picker view selection
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		initElements()
		byTextField.text = pickerData[row]
		attTextField1.placeholder = pickerData[row]
		attTextField2.placeholder = pickerData[row]
		let image = UIImage(systemName: "\(pickerImages[row])")
		attTextField1.leftImage = image
		attTextField2.leftImage = image
		elementPicked = row
	
		if elementPicked == 2{
			attTextField1.keyboardType = .numberPad
			attTextField2.keyboardType = .numberPad
		}
		else{
			attTextField1.keyboardType = .default
			attTextField2.keyboardType = .default
		}
	}
	
	
//MARK: - Buttons
	@IBAction func saveClicked(_ sender: Any) {
		guard !self.attTextField1.text!.isEmpty else {return}
		guard !self.attTextField2.text!.isEmpty else {return}
		let att1 = attTextField1.text!
		let att2 = attTextField2.text!
		var attInt1: Int16! 
		var attInt2: Int16!
		if elementPicked == 2 {
			attInt1 = Int16(attTextField1.text!)!
			attInt2 = Int16(attTextField2.text!)!
		}
		
		
		
		switch (elementPicked) {
			case 0:
				CoreDataController.shared.modifyByName(name1: att1, name2: att2)
			case 1:
				CoreDataController.shared.modifyByAuthor(author1: att1, author2: att2)
			case 2:
				CoreDataController.shared.modifyBynPages(nPages1: attInt1, nPages2: attInt2)
			default:
				print("")
		}
		
		initElements()
	}
	
	@IBAction func printClicked(_ sender: Any) {
		CoreDataController.shared.loadAllBooks()
	}
	
	
//MARK: - Gestures
	@objc func tapView() {
		view.endEditing(true)
	}
	
	@objc
	func leftSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func rightSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func upSwipe(){
		// self.view.backgroundColor = UIColor.yellow
	}
	
	@objc
	func downSwipe(){
		view.endEditing(true)
	}
	
//MARK: - Personal
	func initElements(){
		attTextField1.text = ""
		attTextField2.text = ""
		saveButton.isEnabled = false
		saveButton.alpha = 0.5
	}
	
}
