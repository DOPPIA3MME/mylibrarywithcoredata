//
//  CoreDataController.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 13/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataController {
	//singleton
	static let shared = CoreDataController()
	/*Pensa al Managed Object Context come l’involucro che contiene tutte le istanze delle varie NSManagedObject che conterrà la tua applicazione.*/
	var context: NSManagedObjectContext
	
	private init() {
		let application = UIApplication.shared.delegate as! AppDelegate
		self.context = application.persistentContainer.viewContext
	}
	
	//aggiungo un libro al database
	func addBook(name: String, author: String, nPages: Int) {
	
		let books = loadAllBooks()
		let entity = NSEntityDescription.entity(forEntityName: "Book", in: self.context)
		
		//No doppioni
		if books.contains(where: { $0.name == name && $0.author == author })   {
			print("[CDC] \(name) già presente in memoria.\n")
			return
		}
		else{
			do {
				let newBook = Book(entity: entity!, insertInto: self.context)
				newBook.name = name
				newBook.author = author
				newBook.nPages = Int16(nPages)
				try self.context.save()
			} catch let errore {
				print("[CDC] Problema salvataggio Libro: \(name) in memoria.\n")
				print("Stampo l'errore: \n \(errore) \n")
			}
			print("[CDC] Libro \(name) salvato in memoria correttamente.\n")
		}
	}
	
	
	func modifyByName(name1: String, name2: String) {
		let books = loadBooksFromName(name: name1)
		
		for book in books {
			book.name = name2
		}
		
		do {
			try context.save()
			if (books.count == 1){
				print("[CDC] Libro modificato con successo.\n")
			}
			else if books.count > 1{
				print("[CDC] \(books.count) libri modificati con successo.\n")
			}
			else {
				print("[CDC] Non ci sono libri di nome \(name1).\n")
			}
		} catch {
			print("[CDC] Errore salvataggio libro modificato.\n")
		}
	}
	
	func modifyByAuthor(author1: String, author2: String) {
		let books = loadBooksFromAuthor(author: author1)
		
		for book in books {
			book.author = author2
		}
		
		do {
			try context.save()
			if (books.count == 1){
				print("[CDC] Libro modificato con successo.\n")
			}
			else if books.count > 1{
				print("[CDC] \(books.count) libri modificati con successo.\n")
			}
			else {
				print("[CDC] \(author1) non trovato.\n")
			}
			
		} catch {
			print("[CDC] Errore salvataggio libro modificato.\n")
		}
	}
	
	func modifyBynPages(nPages1: Int16, nPages2: Int16) {
		let books = loadBooksFromnPages(nPages: nPages1)
		
		for book in books {
			book.nPages = nPages2
		}
		
		do {
			try context.save()
			if (books.count == 1){
				print("[CDC] Libro modificato con successo.\n")
			}
			else if books.count > 1{
				print("[CDC] \(books.count) libri modificati con successo.\n")
			}
			else {
				print("[CDC] Non ci sono libri di \(nPages1) pagine.\n")
			}
		} catch {
			print("[CDC] Errore salvataggio libro modificato.\n")
		}
	}
	
	//cancello i libri con il nome scelto
	func deleteBookByName(name: String){
		
		//La fetch fa riferimento alla entity Libro
		let fetch: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		//Predicato in base al nome, è come se avessi scelto where name = name
		let predicate = NSPredicate(format: "name = %@", name)
		//ora la fetch ha il predicato
		fetch.predicate = predicate
		
		//Creo una richiesta di delete passandoci la fetch, in pratica ora la request è: cancella i NSManagedObject della entity Book che hanno il nome = name.
		let request = NSBatchDeleteRequest(fetchRequest: fetch as! NSFetchRequest<NSFetchRequestResult>)
		//Questo comando gestisce il tipo del risultato che avremo una volta eseguita la request, un risultato numerico, per poter sapere quanti items avrò cancellato
		request.resultType = .resultTypeCount
		
		do {
			//Eseguiamo la request e la castiamo a NSBatchDeleteResult per poi stampare il batchDeleteResult.result
			let batchDeleteResult = try context.execute(request) as! NSBatchDeleteResult
			print("La request di batch delete ha eliminato \(batchDeleteResult.result ?? 0) records.\n")
			//context.reset()   //funziona anche senza...
		} catch let errore {
			print("[CDC] Problema eliminazione libro.\n")
			print("Stampo l'errore: \n \(errore) \n")
			
			/*let errore = error as NSError
			print("\(errore), \(errore.userInfo)")  //quale dei due metodi per stampare l'errore è il migliore?
			*/
		}
	}
	
	
	//cancello i libri con l'autore scelto
	func deleteBookByAuthor(author: String){
		
		let fetch: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		let predicate = NSPredicate(format: "author = %@", author)
		fetch.predicate = predicate
		
		
		let request = NSBatchDeleteRequest(fetchRequest: fetch as! NSFetchRequest<NSFetchRequestResult>)
		
		
		request.resultType = .resultTypeCount
		
		do {
			
			let batchDeleteResult = try context.execute(request) as! NSBatchDeleteResult
			print("La request di batch delete ha eliminato \(batchDeleteResult.result ?? 0) records.\n")
			//context.reset()   //funziona anche senza...
		} catch let errore {
			print("[CDC] Problema eliminazione libro.\n")
			print("Stampo l'errore: \n \(errore) \n")
		}
	}
	
	//cancello i libri con l'autore scelto
	func deleteBookBynPages(nPages: Int16){
		
		let fetch: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		let predicate = NSPredicate(format: "nPages = %@", nPages)
		fetch.predicate = predicate
		deleteFromContext(fetch: fetch as! NSFetchRequest<NSFetchRequestResult>)
	}
	
	
	//Cancella tutti i libri
	func deleteAllBook() {
		
		// creiamo la fetch request facendo riferimento alla entity Libro
		let fetch: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		deleteFromContext(fetch: fetch as! NSFetchRequest<NSFetchRequestResult>)
	}
	
	
	func deleteFromContext(fetch: NSFetchRequest<NSFetchRequestResult>){
		//Creo una richiesta di delete passandoci la fetch, in pratica ora la request è: cancella i NSManagedObject della entity Book che hanno il nome = name.
		let request = NSBatchDeleteRequest(fetchRequest: fetch)
		
		//Questo comando gestisce il tipo del risultato che avremo una volta eseguita la request, un risultato numerico, per poter sapere quanti items avrò cancellato
		request.resultType = .resultTypeCount
		
		do {
			//Eseguiamo la request e la castiamo a NSBatchDeleteResult per poi stampare il batchDeleteResult.result
			let batchDeleteResult = try context.execute(request) as! NSBatchDeleteResult
			print("La request di batch delete ha eliminato \(batchDeleteResult.result ?? 0) records.\n")
			//context.reset()   //funziona anche senza...
		} catch {
			let errore = error as NSError
			print("\(errore), \(errore.userInfo)")
		}
	}
	
	//MARK: - Get Data
	//restituisce tutti i libri
	func loadAllBooks() -> [Book] {
		print("[CDC] Recupero tutti i libri dal context")
		
		let request: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		request.returnsObjectsAsFaults = false
		/*let sort = NSSortDescriptor(key: "name", ascending: true)
		request.sortDescriptors = [sort]*/
		
		
		let books = self.loadBooksFromFetchRequest(request: request)
		return books
	}
	
	//restituisce i libri filtrati per nome
	func loadBooksFromName(name: String) -> [Book] {
		print("[CDC] Recupero i libri il cui nome è : \(name)")
		
		//creao una richiesta sulla entity book. E' una richiesta di ritorno dic oggetti, cioè i NSManagedObject
		let request: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		request.returnsObjectsAsFaults = true
		
		//creo un predicato, impongo che l'attributo name della entity sia il parametro del metodo
		let predicate = NSPredicate(format: "name = %@", name)
		//aggiungo il predicato alla richiesta di prima. In pratica ora la richiesta è diventata di prendere gli oggetti book con il nome voluto
		request.predicate = predicate
		
		let books = self.loadBooksFromFetchRequest(request: request)
		
		return books
	}
	
	//restituisce i libri filtrati per autore
	func loadBooksFromAuthor(author: String) -> [Book] {
		print("[CDC] Recupero i libri il cui autore è : \(author)")
		
		let request: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		request.returnsObjectsAsFaults = false
		let predicate = NSPredicate(format: "author= %@", author)
		//prendo i libri con numero di pagine >= 30
		//let predicate2 = NSPredicate(format: "num_pagine >= %@", 30)
		request.predicate = predicate
		
		let books = self.loadBooksFromFetchRequest(request: request)
		
		return books
	}
	
	//restituisce i libri filtrati per nPagine
	func loadBooksFromnPages(nPages: Int16) -> [Book] {
		print("[CDC] Recupero i libri il cui numero di pagine è : \(nPages)")
		
		let request: NSFetchRequest<Book> = NSFetchRequest(entityName: "Book")
		request.returnsObjectsAsFaults = true
		let predicate = NSPredicate(format: "nPages == \(nPages)")
		request.predicate = predicate
		
		let books = self.loadBooksFromFetchRequest(request: request)
		
		return books
	}
	
	//esegue la query al database
	private func loadBooksFromFetchRequest(request: NSFetchRequest<Book>) -> [Book] {
		
		var array = [Book]()
		do {
			//faccio la fetch sul context con la request
			array = try self.context.fetch(request)
			
			guard array.count > 0 else {print("[CDC] Non ci sono elementi da leggere.\n"); return []}
			
			for book in array {
				print("[CDC] Book: \(book.name!) - Author: \(book.author!) - nPages: \(book.nPages)")
			}
			
		} catch let errore {
			print("[CDC] Problema esecuzione FetchRequest.\n")
			print("  Stampo l'errore: \n \(errore) \n")
		}
		
		return array
	}
	
}
