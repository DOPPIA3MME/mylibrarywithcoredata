//
//  TableViewController.swift
//  LibraryWithCoreData
//
//  Created by Maurizio Minieri on 15/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
	
	@IBOutlet var myTableView: UITableView!
	
	var books: [Book]!
	var bookDictionary = [String: [String]]()
	var bookSectionTitles = [String]()
	
	var resultSearchController = UISearchController()
	var array: [(key: String, value: [String])]!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Initialization code
		
		// Uncomment the following line to preserve selection between presentations
		self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		 self.navigationItem.rightBarButtonItem = self.editButtonItem
		
		
	
		/*
		resultSearchController = ({
			let controller = UISearchController(searchResultsController: nil)
			controller.searchResultsUpdater = self
			controller.dimsBackgroundDuringPresentation = false
			controller.searchBar.sizeToFit()
			
			tableView.tableHeaderView = controller.searchBar
			
			return controller
		})()
		*/
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		books = CoreDataController.shared.loadAllBooks()
		bookDictionary.removeAll()
		
		for book in books {
			//prendo solo la prima lettere nel book.name, sarà una key
			//let bookKey = String(book.name!.prefix(1))
			let bookKey = book.author!
			
			//se già esiste la chiave nel dizionario...
			if var bookValues = bookDictionary[bookKey] {
				//in bookValues appendo il bookname e poi lo aggiungo alla chiave
				//print("si, appendo in \(bookValues): \(book.name!)")
				bookValues.append(book.name!)
				bookDictionary[bookKey] = bookValues
			} else {
				//creo la chiave e ci metto il valore
				//print("no, \( bookDictionary[bookKey]) = \([book.name!])")
				bookDictionary[bookKey] = [book.name!]
			}
		}
		
		//prendo le keys e le ordino
		
		bookSectionTitles = [String](bookDictionary.keys)
		bookSectionTitles = bookSectionTitles.sorted(by: { $0 < $1 })
		//print("bookSectionTitles: \(bookSectionTitles)")
		
		tableView.reloadData()
	}
	
	// MARK: - Table View
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		//return the number of sections
		return bookSectionTitles.count
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		// return the number of rows
		
		//section sarà un numero che ogni volta cambierà per quante section ci sono, quindi bookKey sarà prima z, poi s, poi a...
		let bookKey = bookSectionTitles[section]
		//bookValues saranno i valori associati a quella chiave
		if let bookValues = bookDictionary[bookKey] {
			return bookValues.count
		}
		
		return 0

	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		
		
		
		return bookSectionTitles[section]
	}
	
	override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
		view.tintColor = UIColor.darkGray
		let header = view as! UITableViewHeaderFooterView
		header.textLabel?.textColor = UIColor.white
	}
	
	
	//restituisce le sezioni
	override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
		return bookSectionTitles
	}
	
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
		
		//prendo la chiave in base alla section selezionata, per esempio b
		let bookKey = bookSectionTitles[indexPath.section]
		//bookValues sarà l'array ordinato dei valori associati alla chiave, ex bella, bau, ballo
		if let bookValues = bookDictionary[bookKey]?.sorted() {
			
			cell.textLabel?.text = bookValues[indexPath.row]
			cell.textLabel?.textColor = .white
			//cell.imageView?.image = UIImage(named: bookValues[indexPath.row].lowercased())
			//cell.detailTextLabel?.text = "ciao"
			
		}
		
		return cell
		
	}
	
	
	/*
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
	//return 100
	}
	*/
	
	// Override to support conditional editing of the table view.
	override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the specified item to be editable.
		return true
	}
	
	
	
	// Override to support editing the table view.
	override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
		if editingStyle == .delete {
			
			//A me non serve che il dictionary sia in ordine, basta che io prenda le sue chiavi in ordine, quindi mi basta usare il bookSectionTitles
			
			//bookSectionTitles: ["a","s","z"]. Attraverso l'indexPath.section so quale section ho selezionato, cioè quale key
			//print("indexPath.section:\(indexPath.section)")
			let key = bookSectionTitles[indexPath.section]
			//print("bookSectionTitles:\(bookSectionTitles)")
			//print("key: "+key)
			//se ho eliminato una cella nella sezione s allora key = s e quindi vado a prendermi tutti i valori di bookDictionary con key = s, per esempio sara, sole, sale
			let bookValues = bookDictionary[key]
			//print("bookValues:\(bookValues!)")
			//indexPath.row sarà quel numero che rappresenta la riga nella sezione, quindi se ho selezionato sale allora sarà 2. In questo modo infatti vado a prendere array[2], cioè proprio sale
			//print("indexPath.row:\(indexPath.row)")
			let value = bookValues![indexPath.row]
			//print("value: "+value)
			//devo eliminare il valore dal dictionary, pena crash
			bookDictionary = bookDictionary.mapValues{ $0.filter{ $0 != value} }
			CoreDataController.shared.deleteBookByName(name: value)
			tableView.deleteRows(at: [indexPath], with: .fade)
			
		} else if editingStyle == .insert {
			// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
		}
	}
	
	
	
	// Override to support rearranging the table view.
	override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
		
	}
	
	
	
	// Override to support conditional rearranging of the table view.
	override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
		// Return false if you do not want the item to be re-orderable.
		return true
	}
	
	
	
}
