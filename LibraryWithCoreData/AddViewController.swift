//
//  ViewController.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 13/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit


class AddViewController: UIViewController, UITextFieldDelegate {
	
	@IBOutlet weak var nameTextField: UITextField!
	@IBOutlet weak var authorTextField: UITextField!
	@IBOutlet weak var nPagesTextField: UITextField!
	@IBOutlet weak var saveButton: UIButton!
	
	var controllers = [UIViewController]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		nameTextField.delegate = self
		authorTextField.delegate = self
		nPagesTextField.delegate = self
		nPagesTextField.keyboardType = UIKeyboardType.numberPad
		
		saveButton.isEnabled = false
		saveButton.alpha = 0.5
		
		//nameTextField.becomeFirstResponder()
		
	
		
		
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
		//tap.numberOfTapsRequired = 1
		//tap.numberOfTouchesRequired = 1
		//tap.delegate = self
		view.addGestureRecognizer(tap)
		
		let left = UISwipeGestureRecognizer(target : self, action : #selector(leftSwipe))
		left.direction = .left
		self.view.addGestureRecognizer(left)
		
		let right = UISwipeGestureRecognizer(target : self, action : #selector(rightSwipe))
		right.direction = .right
		self.view.addGestureRecognizer(right)
		
		let up = UISwipeGestureRecognizer(target : self, action : #selector(upSwipe))
		up.direction = .up
		self.view.addGestureRecognizer(up)
		
		let down = UISwipeGestureRecognizer(target : self, action : #selector(downSwipe))
		down.direction = .down
		self.view.addGestureRecognizer(down)
		
		// byTextField.addTarget(self, action: #selector(methodName()), for: .editingDidBegin)
	}
	
	override func didReceiveMemoryWarning(){
		super.didReceiveMemoryWarning()
	}

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	
	//MARK: - Text Field
	
	//Grazie al delegate posso usare questa funzione
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		var text1 = nameTextField.text
		var text2 = authorTextField.text
		var text3 = nPagesTextField.text
		
		if textField == nameTextField {
			text1 = (nameTextField.text! as NSString).replacingCharacters(in: range, with: string)
		}
		else if textField == authorTextField {
			text2 = (authorTextField.text! as NSString).replacingCharacters(in: range, with: string)
		}
		else{
			text3 = (nPagesTextField.text! as NSString).replacingCharacters(in: range, with: string)
			
			self.nPagesTextField.keyboardType = UIKeyboardType.numberPad
		}
		
		if text1!.isEmpty || text2!.isEmpty || text3!.isEmpty {
			saveButton.isEnabled = false
			saveButton.alpha = 0.5
		} else {
			saveButton.isEnabled = true
			saveButton.alpha = 1.0
		}
		
		//nameTextField.layer.borderWidth = 1
		
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		
		if textField == nameTextField{
			nameTextField.layer.borderWidth = 2
		}
		else if textField == authorTextField{
			authorTextField.layer.borderWidth = 2
		}
		else{
			nPagesTextField.layer.borderWidth = 2
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		
		if textField == nameTextField{
			nameTextField.layer.borderWidth = 0
		}
		else if textField == authorTextField{
			authorTextField.layer.borderWidth = 0
		}
		else{
			nPagesTextField.layer.borderWidth = 0
		}
	}
	
	
//MARK: - Actions
	
	@IBAction func saveClicked(_ sender: Any) {
		guard !self.nameTextField.text!.isEmpty else {return}
		guard !self.authorTextField.text!.isEmpty else {return}
		guard !self.nPagesTextField.text!.isEmpty else {return}
		
		let name = self.nameTextField.text!
		let author = self.authorTextField.text!
		let nPages = Int(self.nPagesTextField.text!)!
		
		CoreDataController.shared.addBook(name: name, author: author, nPages: nPages)
		
		initElements()
	}
	
	@IBAction func printClicked(_ sender: Any) {
		CoreDataController.shared.loadAllBooks()
		//CoreDataController.shared.loadBooksFromName(name: "Book1")
	}
	
	
//MARK: - Gestures
	@objc func tapView() {
		view.endEditing(true)
	}
	
	@objc
	func leftSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func rightSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func upSwipe(){
		// self.view.backgroundColor = UIColor.yellow
	}
	
	@objc
	func downSwipe(){
		view.endEditing(true)
	}
	
//MARK: - Personal
	func initElements(){
		nameTextField.text = ""
		authorTextField.text = ""
		nPagesTextField.text = ""
		saveButton.isEnabled = false
		saveButton.alpha = 0.5
		nameTextField.layer.borderWidth = 0
	}
	
}
