//
//  DeleteViewController.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 15/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//

import UIKit

class DeleteViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
	
	@IBOutlet weak var byTextField: UITextField!
	@IBOutlet weak var attTextField: DesignableUITextField!
	@IBOutlet weak var saveButton: UIButton!
	
	let byPickerView: UIPickerView = UIPickerView()
	var pickerData: [String] = ["name","author","nPages"]
	var pickerImages: [String] = ["book","person","number.circle"]
	var elementPicked = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.byPickerView.delegate = self
		self.byPickerView.dataSource = self
		attTextField.placeholder = pickerData[0]
		attTextField.delegate = self
		initElements()
		
		byTextField.inputView = byPickerView
		byTextField.text = pickerData[0]
		byTextField.tintColor = UIColor.clear
		
		let tap = UITapGestureRecognizer(target: self, action: #selector(tapView))
		//Uncomment the line below if you want the tap not not interfere and cancel other interactions.
		//tap.cancelsTouchesInView = false
		view.addGestureRecognizer(tap)
		let left = UISwipeGestureRecognizer(target : self, action : #selector(leftSwipe))
		left.direction = .left
		view.addGestureRecognizer(left)
		
		let right = UISwipeGestureRecognizer(target : self, action : #selector(rightSwipe))
		right.direction = .right
		view.addGestureRecognizer(right)
		
		let up = UISwipeGestureRecognizer(target : self, action : #selector(upSwipe))
		up.direction = .up
		view.addGestureRecognizer(up)
		
		let down = UISwipeGestureRecognizer(target : self, action : #selector(downSwipe))
		down.direction = .down
		view.addGestureRecognizer(down)
	}
	
	//MARK: - Text Field
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	//Grazie al delegate posso usare questa funzione
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		var text = attTextField.text
		text = (attTextField.text! as NSString).replacingCharacters(in: range, with: string)
		
		if text!.isEmpty {
			saveButton.isEnabled = false
			saveButton.alpha = 0.5
		} else {
			saveButton.isEnabled = true
			saveButton.alpha = 1.0
		}
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		
		attTextField.layer.borderWidth = 2
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		
		attTextField.layer.borderWidth = 0
	}
	
	//MARK: - Picker View
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return pickerData.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		return pickerData[row]
	}
	
	// Capture the picker view selection
	func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
		initElements()
		byTextField.text = pickerData[row]
		attTextField.placeholder = pickerData[row]
		let image = UIImage(systemName: "\(pickerImages[row])")
		attTextField.leftImage = image
		elementPicked = row
		
		if elementPicked == 2{
			attTextField.keyboardType = .numberPad
		}
		else{
			attTextField.keyboardType = .default
		}
	}
	
	
	
	//MARK: - Outlet actions
	
	@IBAction func saveClicked(_ sender: Any) {
		
		guard !self.attTextField.text!.isEmpty else {return}
		let att = attTextField.text!
		
		switch (elementPicked) {
			case 0:
				CoreDataController.shared.deleteBookByName(name: att)
			case 1:
				CoreDataController.shared.deleteBookByAuthor(author: att)
			case 2:
				//CoreDataController.shared.deleteBookBynPages(nPages: att)
				print("da implementare")
			default:
				print("")
		}
		
		
		initElements()
	}
	
	
	@IBAction func printClicked(_ sender: Any) {
		CoreDataController.shared.loadAllBooks()
	}
	
	@IBAction func deleteAllClicked(_ sender: Any) {
		// create the alert
		let alert = UIAlertController(title: "Notice", message: "Clicking will deleting all your books. Is this what you intended to do?", preferredStyle: .alert)
		
		// add the actions (buttons)
		//alert.addAction(UIAlertAction(title: "Remind Me Tomorrow", style: .default, handler: nil))
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		alert.addAction(UIAlertAction(title: "Delete All", style: .destructive, handler: { action in
			CoreDataController.shared.deleteAllBook()
			print("Deleted all books")
		}))
		
		// show the alert
		self.present(alert, animated: true, completion: nil)
	}
	
	
	//MARK: - Gestures recognizers
	
	
	@objc func tapView() {
		view.endEditing(true)
	}
	
	@objc
	func leftSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func rightSwipe(){
		view.endEditing(true)
	}
	
	@objc
	func upSwipe(){
		// self.view.backgroundColor = UIColor.yellow
	}
	
	@objc
	func downSwipe(){
		view.endEditing(true)
	}
	
	//MARK: - Personal
	func initElements(){
		attTextField.text = ""
		saveButton.isEnabled = false
		saveButton.alpha = 0.5
	}
	
}
