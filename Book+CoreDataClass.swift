//
//  Book+CoreDataClass.swift
//  ProjectWithCoreData
//
//  Created by Maurizio Minieri on 13/12/2019.
//  Copyright © 2019 Maurizio Minieri. All rights reserved.
//
//Created with (Editor\Create NSManagedObject Subclass) then in group select the folder

import Foundation
import CoreData

@objc(Book)
public class Book: NSManagedObject {
    
    override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
            super.init(entity: entity, insertInto: context)
    }
}
